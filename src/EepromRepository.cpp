#include "EepromRepository.h"

EepromRepository::EepromRepository()
{
    this->jsonDocument = new DynamicJsonDocument(EEPROM_SIZE);
    this->isInitialized = false;
}

EepromRepository::~EepromRepository()
{
}

void EepromRepository::begin()
{
    if (isInitialized) return;

    String json = this->readEEPROM();

    deserializeJson(*jsonDocument, json);
    this->jsonObject = this->jsonDocument->as<JsonObject>();

    // If JSON couldn't be parsed,
    if (this->jsonObject.isNull()) {
        clear();
    }

    isInitialized = true;
}

bool EepromRepository::exists(String key)
{
    return jsonObject.containsKey(key);
}

void EepromRepository::clear()
{
    this->jsonDocument->clear();

    jsonObject = this->jsonDocument->to<JsonObject>();
}

void EepromRepository::clearEEPROM()
{
    #if defined(ESP8266) || defined(ESP32)
        EEPROM.begin(EEPROM_SIZE);
    #endif

    // Write all bytes to zero.
    for (int i = 0; i < EEPROM_SIZE; i++) EEPROM.write(i, '\0');

    EEPROM.end();
}

void EepromRepository::remove(String key)
{
    this->jsonObject.remove(key);
}

void EepromRepository::apply()
{
    clearEEPROM();

    String json;
    serializeJson(*jsonDocument, json);

    writeEEPROM(json);
}

String EepromRepository::readEEPROM()
{
    #if defined(ESP8266) || defined(ESP32)
        EEPROM.begin(EEPROM_SIZE);
    #endif

    String json = "";
    unsigned char byte;
    
    for (unsigned int i = 0; i < EEPROM_SIZE; i++) {
        byte = EEPROM.read(i);
        if (byte == '\0') {
            break;
        } else {
            json += char(byte);
        }
    }

    EEPROM.end();

    Serial.println("EEPROM data:");
    Serial.println(json);

    return json;
}

void EepromRepository::writeEEPROM(String json) 
{
            #if defined(ESP8266) || defined(ESP32)
            // Begin EEPROM library.
            EEPROM.begin(EEPROM_SIZE);
            #endif

            // Write JSON data.
            unsigned int i;
            for (i = 0; i < json.length(); i++) {
                    EEPROM.write(i, json.charAt(i));
            }

            // If the length of the JSON data is smaller than the EEPROM size,
            if (json.length() < EEPROM_SIZE)
                    // than write at the last position a null-termination.
                    EEPROM.write(json.length(), '\0');

            // End EEPROM library.
            EEPROM.end();
    }