#ifndef __EEPROM_REPOSITORY_H__
#define __EEPROM_REPOSITORY_H__

#include <Arduino.h>
#include <EEPROM.h>
#include <string.h>
#include <ArduinoJson.h>
#include "../include/consts.h"

#ifndef EEPROM_SIZE
  #if defined(ESP8266) || defined(ESP32) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    #define EEPROM_SIZE 4096
  #endif
  #if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega32U4__)
    #define EEPROM_SIZE 1024
  #endif
  #if defined(__AVR_ATmega168__)
    #define EEPROM_SIZE 512
  #endif
#endif

class EepromRepository
{
    DynamicJsonDocument *jsonDocument;
    JsonObject jsonObject;
    bool isInitialized;

    void clearEEPROM();
    String readEEPROM();
    void writeEEPROM(String json);

public:
    EepromRepository();
    ~EepromRepository();
    String get(String id);
    void begin();
    void clear();
    void remove(String key);
    bool exists(String key);
    void apply();
    
    template <typename T>
    T get(String key) {
        T value = jsonObject[key].as<T>();
        return value;
    };
    template <typename T>
    void set(String key, T value) {
            jsonObject[key] = value;
    };
};

static EepromRepository EepromStorage;
extern EepromRepository EepromStorage;

#endif